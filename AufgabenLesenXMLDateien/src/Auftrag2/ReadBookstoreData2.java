package Auftrag2;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

/* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                        titel:  Java ist auch eine Insel 
 *                        autor:  Max Mustermann 
 */



public class ReadBookstoreData2 {

	public static void main(String[] args) {

		
		try {

			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			// Add your code here
			DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
			DocumentBuilder dB = dBF.newDocumentBuilder();
			Document d = dB.parse("buchhandlung2.xml");
			
			System.out.println("Titel:" + d.getElementsByTagName("titel").item(0).getTextContent());
			System.out.println("Autor: " + d.getElementsByTagName("autor").item(0).getTextContent());
			
		} catch (Exception e) {
		}

	}

}
