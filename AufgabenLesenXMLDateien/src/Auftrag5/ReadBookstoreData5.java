package Auftrag5;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/* Arbeitsauftrag:  Lesen Sie nur die Autoren des Buches "XQuery Kick Start" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  
 *                   Ausgabe soll wie folgt aussehen:
 *                     Buchtitel:  XQuery Kick Start
 *                     Autoren: 
 *                     	    1. autor: James McGovern
 *                          2. autor: Per Bothner
 *                          3. autor: Kurt Cagle
 *                          4. autor: James Linn
 *                          5. autor: Vaidyanathan Nagarajan
 *                          
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */



public class ReadBookstoreData5 {

	public static void main(String[] args) {

		
			// Name der Datei: "src/Auftrag5/buchhandlung.xml"
			// Add your code here
		
		try {
			
			DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
			DocumentBuilder dB = dBF.newDocumentBuilder();
			Document d = dB.parse("buchhandlung4_5.xml");
		
			NodeList titelList = d.getElementsByTagName("titel");
			NodeList autorList = d.getElementsByTagName("autor");
			
			for(int i = 0; i < titelList.getLength(); i++) {
				if(titelList.item(i).getTextContent().equals("XQuery Kick Start")) {
					System.out.println("Buchtitel: " + titelList.item(i).getTextContent());
					System.out.println("Autoren:");
					int len = titelList.getLength() - i;
					for(int j = i; j < autorList.getLength() - len + 1; j++) {
						System.out.println("\t" + (j - i + 1) + ". "+ autorList.item(j).getNodeName() + ": " + autorList.item(j).getTextContent());
					}
				}
					
			}
			
							
						
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

}
