package Auftrag4;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/* Arbeitsauftrag:  Lesen Sie nur die Buchtitel 
 *					aus der Datei "buchhandlung.xml" und geben Sie sie 
 * 					auf dem Bildschirm aus.
 * 
 *                  Ausgabe soll wie folgt formatiert werden:
 *                     1. titel: Everyday Italian
 *                     2. titel: Harry Potter
 *                     3. titel: XQuery Kick Start
 *                     4. titel: Learning XML
 *                     
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */



public class ReadBookstoreData4 {

	public static void main(String[] args) {

			// Name der Datei: "src/Auftrag4/buchhandlung.xml"
			// Add your code here
			try {
				
				DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
				DocumentBuilder dB = dBF.newDocumentBuilder();
				Document d = dB.parse("buchhandlung4_5.xml");
				
				NodeList titelList = d.getElementsByTagName("titel");
				
				for(int i = 0; i < titelList.getLength(); i++)
					System.out.println( i + 1 + ". " + titelList.item(i).getNodeName() + ": " + titelList.item(i).getTextContent());
				
			} catch (Exception e) {
				e.printStackTrace();
			}



	}

}
