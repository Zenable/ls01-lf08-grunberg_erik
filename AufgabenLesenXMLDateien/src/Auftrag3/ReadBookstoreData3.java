package Auftrag3;

 /* Arbeitsauftrag:  Lesen Sie alle Angaben des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                    titel:  Java ist auch eine Insel   
 *					  vorname:  Christian 
 *                    nachname:  Ullenboom 
 */

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData3 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			// Add your code here
			
			DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
			DocumentBuilder dB = dBF.newDocumentBuilder();
			Document d = dB.parse("buchhandlung3.xml");
			
			NodeList titelList = d.getElementsByTagName("titel");
			NodeList vornameList = d.getElementsByTagName("vorname");
			NodeList nameList = d.getElementsByTagName("nachname");
			
			for(int i = 0; i < titelList.getLength(); i++)
				System.out.println(titelList.item(i).getNodeName() + ": " + titelList.item(i).getTextContent());
			
			for(int i = 0; i < vornameList.getLength(); i++)
				System.out.println(vornameList.item(i).getNodeName() + ": " + vornameList.item(i).getTextContent());
			
			for(int i = 0; i < nameList.getLength(); i++)
				System.out.println(nameList.item(i).getNodeName() + ": " +  nameList.item(i).getTextContent());
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}
