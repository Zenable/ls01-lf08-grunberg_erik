package Auftrag1;

import java.io.BufferedReader;

import javax.xml.parsers.*;
import org.w3c.dom.*;


public class ReadBookstoreData1 {

  public static void main(String[] args) {

    try {
    	// Name der Datei: "src/Auftrag1/buchhandlung.xml"
    	// Add your code here
    	DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
		DocumentBuilder dB = dBF.newDocumentBuilder();
		Document d = dB.parse("buchhandlung1.xml");
		
		System.out.println("Titel: " + d.getElementsByTagName("titel").item(0).getTextContent());

    } catch (Exception e) {
      e.printStackTrace();
    } 

  }

}
