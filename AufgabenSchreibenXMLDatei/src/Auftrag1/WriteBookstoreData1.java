package Auftrag1;
/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteBookstoreData1 {

	public static void main(String[] args) {

		//Add your code here
		
		try {
			DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
			DocumentBuilder dB = dBF.newDocumentBuilder();
			Document d = dB.newDocument();
			
			Element buchhandlung = d.createElement("buchhandlung");
			d.appendChild(buchhandlung);
			
			Element buch = d.createElement("Buch");
			buchhandlung.appendChild(buch);
			
			Element titel = d.createElement("titel");
			buch.appendChild(titel);
			
			titel.appendChild(d.createTextNode("Java ist auch eine Insel"));
			
			
			TransformerFactory tF = TransformerFactory.newInstance();
			Transformer t = tF.newTransformer();
			
			DOMSource xmlSource = new DOMSource(d);
			StreamResult sR = new StreamResult(new File("buchhandlung.xml"));
			
			t.transform(xmlSource, sR);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
