package Auftrag2;
/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteBookstoreData2 {

	public static void main(String[] args) {

		try {
			DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
			DocumentBuilder dB = dBF.newDocumentBuilder();
			Document d = dB.newDocument();
			
			Element buchhandlung = d.createElement("buchhandlung");
			d.appendChild(buchhandlung);
			
			Element buch = d.createElement("buch");
			buchhandlung.appendChild(buch);
			
			Element titel = d.createElement("titel");
			buch.appendChild(titel);
			titel.appendChild(d.createTextNode("Java ist auch eine Insel"));
			
			Element autor = d.createElement("autor");
			buch.appendChild(autor);
			autor.appendChild(d.createTextNode("Christian Ullenboom"));
			
			DOMSource xmlSource = new DOMSource(d);
			StreamResult sR = new StreamResult(new File("buchhandlung.xml"));
			
			TransformerFactory tF = TransformerFactory.newInstance();
			Transformer t = tF.newTransformer();
			
			t.setOutputProperty(OutputKeys.INDENT,"yes");
			
			t.transform(xmlSource, sR);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
