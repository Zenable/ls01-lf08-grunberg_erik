package readBookstoreData;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* Arbeitsauftrag:  Lesen Sie den Titel des Buches "Java ist auch eine Insel" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  Ausgabe soll wie folgt aussehen:
 *                     titel:  Java ist auch eine Insel                  
 */




public class ReadBookstoreData1 {

	public static void main(String[] args) {
		try {
			DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
			DocumentBuilder dB = dBF.newDocumentBuilder();
			Document d = dB.parse("buchhandlung.xml");
			
			
			Element titelE = (Element) d.getElementsByTagName("titel").item(0);
			Element titelE2 = (Element) d.getElementsByTagName("titel").item(1);
			Element autorE = (Element) d.getElementsByTagName("autor").item(0);
			Element autorE2 = (Element) d.getElementsByTagName("autor").item(1);
			System.out.print(titelE.getTextContent());
			System.out.print(titelE2.getTextContent());
			System.out.print(autorE.getTextContent());
			System.out.print(autorE2.getTextContent());
			
			
				
		} catch (Exception e) {
			System.out.println("Datei konnte nicht eingelesen werden!");
		}
		
		
		

	}

}
