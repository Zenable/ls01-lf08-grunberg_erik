package csvDateiLesenBeispiel;

import java.io.BufferedReader;
import java.io.FileReader;

public class CSVDateiLesenBeispiel {

	public static void main(String[] args) {
		try {
			FileReader fr = new FileReader("artikelliste.csv");
			BufferedReader br = new BufferedReader(fr);
			while(br.ready())
				System.out.println(br.readLine());
			
		} catch (Exception e) {
			System.out.println("Error: Datei konnte nicht eingelesen werden. ");
		}
		
	}

}
