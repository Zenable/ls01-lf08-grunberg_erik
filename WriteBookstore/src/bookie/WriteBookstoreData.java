package bookie;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteBookstoreData {

	public static void main(String[] args) {
		
		try {
			DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
			DocumentBuilder dB = dBF.newDocumentBuilder();
			Document d = dB.newDocument();
			
			Element buecher = d.createElement("buecher");
			d.appendChild(buecher);
			
			Element buch = d.createElement("buch");
			buecher.appendChild(buch);
			
			Element titel = d.createElement("titel");
			buch.appendChild(titel);
			
			titel.appendChild(d.createTextNode("Java ist auch eine Insel"));
			
			
			
			TransformerFactory tF = TransformerFactory.newInstance();
			Transformer t = tF.newTransformer();
			
			DOMSource xmlSource = new DOMSource(d);
			StreamResult outputTarget = new StreamResult(new File("buchhandlung.xml"));
			
			t.transform(xmlSource, outputTarget);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
