package de.oszimt.ls81.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dariush
 *
 */
public class CSVHandler {

  /**
   * muss sich im aktuellen Ordner befinden!
   */
  private String file;
  private String delimiter;
  private String line = "";

  /**
   * Default constructor
   */
  public CSVHandler() {
    this(";", "studentNameCSV.csv");
  }

  /**
   * Standard constructor
   * 
   * @param delimiter, Trennzeichen
   * @param file,      Datei zum Einlesen
   */
  // Constructor 2
  public CSVHandler(String delimiter, String file) {
    super();
    this.delimiter = delimiter;
    this.file = file;
  }

  /**
   * Liest alle Schüler aus der csv aus und gibt sie zurück
   * 
   * @return List mit Schülern
   */
  public List<Schueler> getAll() {
    Schueler s = null;
    List<Schueler> students = new ArrayList<Schueler>();

    try {
      BufferedReader br = new BufferedReader(new FileReader("studentNameCSV.csv"));

      while(br.ready()) {
    	  String[] bla = br.readLine().split(";");
    	  if(!bla[0].equals("Vorname"))
    		 students.add(new Schueler(bla[0],Integer.parseInt(bla[2]),Integer.parseInt(bla[3]), Integer.parseInt(bla[4])));
    	  
      }

    } catch (IOException e) {
    	System.out.println("Datei konnte nicht eingelesen werden");

    }

    return students;
  }

  /**
   * Gibt alle Schüler aus
   * 
   * @param students
   */
  public void printAll(List<Schueler> students) {
    for (Schueler s : students) {
      System.out.println(s.getName());
    }
  }
}
