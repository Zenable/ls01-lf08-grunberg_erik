package Auftrag2;

import java.io.FileWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

/* Auftrag 2) Erstellen Sie folgende book2.json:
{
	"titel": "Java ist auch eine Insel",
	"jahr": 1998,
	"preis": 29.95,
	"autor": "Christian Ullenboom"
}
*/
public class WriteBookDataJason {

	public static void main(String[] args) {

		Buch b1 = new Buch("Java ist auch eine Insel", 1998, 29.95, "Christian Ullenboom");
		
		JsonObjectBuilder jOB = Json.createObjectBuilder();
		jOB.add("titel", b1.getTitel());
		jOB.add("jahr", b1.getJahr());
		jOB.add("preis", b1.getPreis());
		jOB.add("autor", b1.getAutor());
		
		JsonObject jO = jOB.build();
		
		try {
			FileWriter fW = new FileWriter("book2.json");
			JsonWriter jW = Json.createWriter(fW);
			jW.write(jO);
			jW.close();
			fW.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
