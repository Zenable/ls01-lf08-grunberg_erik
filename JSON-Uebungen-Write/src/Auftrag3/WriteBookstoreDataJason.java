package Auftrag3;

import java.io.FileWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

/* Auftrag 3) Erstellen Sie notwendige Klassen und erstellen Sie folgende buchhandlung3.json:
{
	"name": "OSZIMT Buchhandlung",
	"tel": "030-225027-800",
	"fax": "030-225027-809",
	"adresse": {
		"strasse": "Haarlemer Strasse",
		"hausnummer": "23-27",
		"plz": "12359",
		"ort": "Berlin"
	}
}

*/

public class WriteBookstoreDataJason {

	public static void main(String[] args) {
		
		Buchhandlung bH = new Buchhandlung("OSZIMT Buchhandlung", "030-225027-800", "030-225027-809", new Adresse("Haarlemer Strasse", "23-27", "12359", "Berlin"));
		
		JsonObjectBuilder jOB = Json.createObjectBuilder();
		JsonObjectBuilder jOBA = Json.createObjectBuilder();
		
		jOB.add("name", bH.getName());
		jOB.add("tel", bH.getTel());
		jOB.add("fax", bH.getFax());
		
		jOBA.add("strasse", bH.getAdresse().getStrasse());
		jOBA.add("hausnummer", bH.getAdresse().getHausnummer());
		jOBA.add("plz", bH.getAdresse().getPlz());
		jOBA.add("ort", bH.getAdresse().getOrt());
		jOB.add("adresse", jOBA);
		
		JsonObject jO = jOB.build();
		
		
		try {
			FileWriter fW = new FileWriter("buchhandlung3.json");
			JsonWriter jW = Json.createWriter(fW);
			jW.write(jO);
			jW.close();
			fW.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
